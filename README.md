
# Exercices de SQL

A partir d'un jeu de données existant, réaliser des requêtes SQL permettant d'extraire des informations intéressantes.

- Source : https://www.sqltutorial.org/sql-sample-database/

![Schéma](https://www.sqltutorial.org/wp-content/uploads/2016/04/SQL-Sample-Database-Schema.png)


## Installer ce projet
Importez les fichiers SQL suivants : 
 - [Structure](./structure.sql)
 - [Data](./data.sql)

Attention, le fichier n'est pas venu avec la création de la base de données en elle-même. Ainsi assurez-vous d'en créer une . 

Il est possible d'importer les fichiers en ligne de commande. En voici un exemple
> mysql.exe -uroot ressources-humaines < sql-ressources-humaines/data.sql

## Réaliser les requêtes suivantes :

L'objectif est de réaliser la requête sql associée à la question. 
Réalisez la requête SQL même si elle ne retourne aucun enregistrement actuellement.

### 1. Listez les différents types de job title existants au sein de l'entreprise
Il est attendu de n'avoir aucun doublons.

### 2. Qui n'a pas de supérieur hiérarchique (prénom et nom)? 

### 3. Existerait-il des employés ayant un salaire supérieur ou inférieur à la grille salariale ? 

### 4. Combien d'employés n'ont pas d'enfant (pas de personne asssociée dans la table dependents)

### 5. Listez le nom d'un employé (prénom et nom) fusionné en une seule colonne nommé full_name, son salaire et le nom de son supérieur hiérarchique


### 6. Comptez le nombre d'employés, ainsi que la charge salariale (somme des salaires) regroupés par région dans le monde

### 7. Quel est le métier le moins rémunérateur (Uniquement basé sur la moyenne entre le salaire max et le salaire minimum présent au sein de la table jobs) ?

### 8. Quelle est la moyenne d'ancienneté (en annuités) des salariés regroupés par département?

### 9. Quel sont les noms des managers ayant le plus de personnes sous leurs tutelle ? 
```sql
```

### 10. Quel est l' employé à qui nous devons souhaiter le prochain anniversaire professionnel ?

## Implémentez les contraintes suivantes:
 - Pour contraindre les RH, le salaire d'un employé (employees.salary) doit toujours respecter les contraintes salariales de son poste (jobs.min_salary ET jobs.max_salary)
 - Implémentez une routine qui supprime tous les employés qui n'ont pas une adresse mail finissant par `@sqltutorial.org`.